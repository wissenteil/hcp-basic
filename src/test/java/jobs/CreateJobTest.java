package jobs;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.example.pages.JobPage;
import org.example.pages.LandingPage;
import org.example.pages.NewJobPage;
import org.example.pages.SignInPage;

import java.util.Random;
import java.util.concurrent.TimeUnit;

import static org.assertj.core.api.Assertions.assertThat;

public class CreateJobTest {


    private static final String BASE_URL = "https://pro.housecallpro-qa.com/pro/log_in";
    private WebDriver driver;
    private SignInPage signIn;
    private LandingPage landingPage;
    private NewJobPage newJobPage;
    private JobPage jobPage;


    @BeforeEach
    public void setUp() {
        System.setProperty("webdriver.gecko.driver", "C:\\Users\\Lenovo-P50\\drivers\\geckodriver.exe");
        driver = new FirefoxDriver();

        signIn = new SignInPage(driver);
        landingPage = new LandingPage(driver);
        newJobPage = new NewJobPage(driver);
        jobPage = new JobPage(driver);
    }

    @AfterEach
    public void teardown() {
        driver.quit();
    }

    @Test
    public void shouldCreateNewJob() {

        String customerLastName = "Customer-" + new Random().nextInt(1000000);
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        driver.get(BASE_URL);

        // Sign in
        signIn.submit("johny@bravo.com", "Happytesting100!");

        // Create Job for new customer
        landingPage.selectNewJob();

        newJobPage.pickDateFrom("28");
        newJobPage.pickDateTo("28");
        newJobPage.createCustomer("Happy", customerLastName);
        newJobPage.enterItemName("Fix sink leakage");
        newJobPage.addPrivateNotes("Private is private");

        newJobPage.save();

        //Assert job is created
        assertThat(jobPage.getItemNameValue()).isEqualTo("Fix sink leakage");
        assertThat(jobPage.getPrivateNotesValue()).isEqualTo("Private is private");
    }
}
