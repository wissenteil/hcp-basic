package org.example;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        plugin = {"pretty", "html:target/html-reports.html"},
        features = "src/main/resources/features",
        glue = {"org/example/setepdefinitions", "org.example.spring",}
)
public class RunCucumberTests {
}
