package org.example.setepdefinitions;

import io.cucumber.java.After;
import io.cucumber.java.Before;
import org.openqa.selenium.WebDriver;

import java.util.concurrent.TimeUnit;

public class Hooks {
    private final WebDriver driver;

    public Hooks(WebDriver driver) {
        this.driver = driver;
    }

    @Before
    public void beforeScenario() {
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.manage().window().maximize();
    }

    @After
    public void afterScenario() {
        driver.quit();
    }
}
