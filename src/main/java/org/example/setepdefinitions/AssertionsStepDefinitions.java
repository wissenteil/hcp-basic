package org.example.setepdefinitions;

import io.cucumber.java.en.Then;
import org.example.model.domain.Job;
import org.example.pages.JobPage;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class AssertionsStepDefinitions {
    private final JobPage jobPage;

    public AssertionsStepDefinitions(JobPage jobPage) {
        this.jobPage = jobPage;
    }

    @Then("^job is saved for ([\\w]+) as follows:$")
    public void assertJob(String customerName, List<Job> jobs) {
        Job job = jobs.get(0);
        assertThat(jobPage.getItemNameValue()).isEqualTo(job.getItemName());
        assertThat(jobPage.getPrivateNotesValue()).isEqualTo(job.getPrivateNotes());
    }
}
