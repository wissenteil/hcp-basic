package org.example.setepdefinitions;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.When;
import org.example.configuration.HcpProperties;
import org.example.model.domain.Job;
import org.example.pages.LandingPage;
import org.example.pages.NewJobPage;
import org.example.pages.SignInPage;
import org.example.utils.DateHelper;
import org.openqa.selenium.WebDriver;

import java.util.List;
import java.util.Random;

public class OperationsStepDefinitions {
    private final HcpProperties properties;
    private final WebDriver driver;
    private final SignInPage signInPage;
    private final LandingPage landingPage;
    private final NewJobPage newJobPage;

    public OperationsStepDefinitions(HcpProperties properties, WebDriver driver, SignInPage signInPage, LandingPage landingPage, NewJobPage newJobPage) {
        this.properties = properties;
        this.driver = driver;
        this.signInPage = signInPage;
        this.landingPage = landingPage;
        this.newJobPage = newJobPage;
    }

    @Given("^a customer is on sign in page$")
    public void navigateToSignInPage() {
        driver.get(properties.getHttp().getBaseUrl());
    }

    @Given("^a logged in customer ([\\w ]+)$")
    public void signCustomerIn(String customerName) {
        signInPage.submit(properties.getUser().getEmail(), properties.getUser().getPassword());
    }

    @When("^([\\w]+) creates following job for his new customer$")
    public void createJob(String customerName, List<Job> jobs) {
        Job job = jobs.get(0);
        String customerLastName = "Customer-" + new Random().nextInt(1000000);
        landingPage.selectNewJob();
        newJobPage.pickDateFrom(parseDateExpression(job.getDateFrom()));
        newJobPage.pickDateTo(parseDateExpression(job.getDateTo()));
        newJobPage.createCustomer("Happy", customerLastName);
        newJobPage.enterItemName(job.getItemName());
        newJobPage.addPrivateNotes(job.getPrivateNotes());

        newJobPage.save();
    }

    private String parseDateExpression(String date) {
        if (date.equals("Next working day")) {
            return DateHelper.getNextWorkingDay();
        } else {
            return date;
        }
    }
}
