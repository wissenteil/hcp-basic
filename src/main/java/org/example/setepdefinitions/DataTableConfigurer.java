package org.example.setepdefinitions;

import io.cucumber.java.DataTableType;
import org.example.model.domain.Job;

import java.util.Map;

public class DataTableConfigurer {
    @DataTableType
    public Job job(Map<String, String> entry) {
        return new Job(
                entry.get("Item name"),
                entry.get("Private notes"),
                entry.get("Date from"),
                entry.get("Date to")
        );
    }
}
