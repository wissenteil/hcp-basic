package org.example.model.domain;

public class Job {

    private final String itemName;
    private final String privateNotes;
    private final String dateFrom;
    private final String dateTo;

    public Job(String itemName, String privateNotes, String dateFrom, String dateTo) {
        this.itemName = itemName;
        this.privateNotes = privateNotes;
        this.dateFrom = dateFrom;
        this.dateTo = dateTo;
    }

    public String getItemName() {
        return itemName;
    }

    public String getPrivateNotes() {
        return privateNotes;
    }

    public String getDateFrom() {
        return dateFrom;
    }

    public String getDateTo() {
        return dateTo;
    }
}
