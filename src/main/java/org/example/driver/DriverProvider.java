package org.example.driver;

import org.example.configuration.HcpProperties;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class DriverProvider {
    private final HcpProperties properties;

    public DriverProvider(HcpProperties properties) {
        this.properties = properties;
    }

    public WebDriver provideFirefoxDriver() {
        System.setProperty("webdriver.gecko.driver", properties.getDriver().getFirefoxPath());
        return new FirefoxDriver();
    }

    public WebDriver provideChromeDriver() {
        System.setProperty("webdriver.chrome.driver", properties.getDriver().getChromePath());
        return new ChromeDriver();
    }
}
