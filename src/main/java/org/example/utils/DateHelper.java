package org.example.utils;

import java.time.Instant;
import java.util.Calendar;
import java.util.Date;

public class DateHelper {

    public static String getNextWorkingDay() {

        Date date = Date.from(Instant.now());
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        do {
            calendar.add(Calendar.DATE, 1);
        } while (calendar.get(Calendar.DAY_OF_WEEK) == 1 || calendar.get(Calendar.DAY_OF_WEEK) == 7);

        return String.valueOf(calendar.get(Calendar.DAY_OF_MONTH));
    }
}
