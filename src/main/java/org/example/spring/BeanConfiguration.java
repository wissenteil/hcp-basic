package org.example.spring;

import org.example.configuration.HcpProperties;
import org.example.driver.DriverProvider;
import org.openqa.selenium.WebDriver;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Configuration
@ComponentScan(basePackages = "org.example")
public class BeanConfiguration {

    @Bean
    @Profile("firefox")
    public WebDriver firefoxDriver(HcpProperties properties) {
        return new DriverProvider(properties).provideFirefoxDriver();
    }

    @Bean
    @Profile("chrome")
    public WebDriver chromeDriver(HcpProperties properties) {
        return new DriverProvider(properties).provideChromeDriver();
    }
}
