package org.example.spring;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;

@SpringBootApplication
@Import(BeanConfiguration.class)
public class CommonApplicationContext {
}
