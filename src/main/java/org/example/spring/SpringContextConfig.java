package org.example.spring;

import io.cucumber.spring.CucumberContextConfiguration;
import org.example.RunCucumberTests;
import org.example.configuration.HcpProperties;
import org.example.pages.JobPage;
import org.example.pages.LandingPage;
import org.example.pages.NewJobPage;
import org.example.pages.SignInPage;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;

@CucumberContextConfiguration
@SpringBootTest(
        classes = {
                BeanConfiguration.class,
                HcpProperties.class,
                RunCucumberTests.class,
                JobPage.class,
                LandingPage.class,
                NewJobPage.class,
                SignInPage.class,
        }
)
@ContextConfiguration
public class SpringContextConfig {
}
