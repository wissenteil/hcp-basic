package org.example.configuration;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties("hcp")
public class HcpProperties {

    private Http http;
    private User user;
    private Driver driver;




    public Http getHttp() {
        return http;
    }

    public User getUser() {
        return user;
    }

    public Driver getDriver() {
        return driver;
    }

    public void setHttp(Http http) {
        this.http = http;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public void setDriver(Driver driver) {
        this.driver = driver;
    }

    public static class Http {
        private String baseUrl;

        public String getBaseUrl() {
            return baseUrl;
        }

        public void setBaseUrl(String baseUrl) {
            this.baseUrl = baseUrl;
        }
    }

    public static class User {

        private String email;
        private String password;

        public String getEmail() {
            return email;
        }

        public String getPassword() {
            return password;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public void setPassword(String password) {
            this.password = password;
        }
    }

    public static class Driver {

        private String chromePath;
        private String firefoxPath;

        public String getChromePath() {
            return chromePath;
        }

        public String getFirefoxPath() {
            return firefoxPath;
        }

        public void setChromePath(String chromePath) {
            this.chromePath = chromePath;
        }

        public void setFirefoxPath(String firefoxPath) {
            this.firefoxPath = firefoxPath;
        }
    }
}
