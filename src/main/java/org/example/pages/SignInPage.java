package org.example.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class SignInPage {

    public SignInPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

    @FindBy(xpath ="//*[@id='email']")
    private WebElement emailInput;

    @FindBy(xpath ="//*[@id='password']")
    private WebElement passwordInput;

    @FindBy(xpath = "//span[contains(@class, 'MuiButton-label') and text() = 'Sign in']")
    private WebElement submit;

    public void submit(String email, String password) {
        emailInput.sendKeys(email);
        passwordInput.sendKeys(password);
        submit.click();
    }
}
