package org.example.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class LandingPage {

    public LandingPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

    @FindBy(xpath = "//span[contains(@class, 'MuiButton-label') and text() = 'NEW']")
    private WebElement newButton;

    @FindBy(xpath = "//span[contains(@class, 'MuiListItemText') and text() = 'Job']")
    private WebElement jobMenuEntry;

    public void selectNewJob() {
        newButton.click();
        jobMenuEntry.click();

    }
}
