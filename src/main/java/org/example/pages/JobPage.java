package org.example.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

public class JobPage {

    public JobPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

    @FindBy(xpath ="//input[contains(@aria-label,'Item name')]")
    private List<WebElement> itemNameEntries;

    @FindBy(xpath ="//div[./div/div/span/h6/span[contains(text(),'Private notes')]]/div[contains(@class,'MuiCardContent-root')]/div/div/div/div/p/span")
    private WebElement privateNotes;

    public String getItemNameValue() {
        return itemNameEntries.get(0).getAttribute("value");
    }

    public String getPrivateNotesValue() {
        return privateNotes.getText();
    }
}
