package org.example.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

public class NewJobPage {

    private final WebDriver driver;

    public NewJobPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    @FindBy(xpath = "//div[contains(@class, 'MuiGrid-container') and contains(., 'From')]//input[@id='input-date']")
    private List<WebElement> dateInputs;

    @FindBy(xpath = "//td[contains(@class,'CalendarDay')]/div[text() ='28']")
    private List<WebElement> calendarDays;

    @FindBy(xpath = "//*[@id='item-name-label']")
    private WebElement itemNameField;

    @FindBy(xpath = "//p[contains(text(),'Private notes')]")
    private WebElement privateNotesBlock;

    @FindBy(xpath = "//div[./div/div/span/h6/span[contains(text(),'Private notes')]]//textarea[1]")
    private WebElement privateNotesInput;

    @FindBy(xpath = "//button[./span[contains(text(),'Save job')]]")
    private WebElement saveJobButton;

    // Customer form
    @FindBy(xpath = "//span[contains(@class, 'MuiButton-label') and text() = '+ New customer']")
    private WebElement newCustomerButton;

    @FindBy(xpath = "//*[@id='customer-dialog-first-name']")
    private WebElement newCustomerFirstName;

    @FindBy(xpath = "//*[@name='last_name']")
    private WebElement newCustomerLastName;

    @FindBy(xpath = "//span[contains(@class, 'MuiButton-label') and text() = 'create']")
    private WebElement submitCustomer;

    public void pickDateFrom(String day) {
        Actions dateActions = new Actions(driver);
        dateActions.moveToElement(dateInputs.get(0)).click().perform();
        dateActions.moveToElement(dateInputs.get(0)).click().perform();
        driver.findElements(By.xpath(String.format("//td[contains(@class,'CalendarDay')]/div[text() ='%s']", day))).get(1).click();
    }

    public void pickDateTo(String day) {
        Actions dateActions = new Actions(driver);
        dateActions.moveToElement(dateInputs.get(1)).click().perform();
        dateActions.moveToElement(dateInputs.get(1)).click().perform();
        driver.findElements(By.xpath(String.format("//td[contains(@class,'CalendarDay')]/div[text() ='%s']", day))).get(1).click();
    }

    public void enterItemName(String itemName) {
        Actions itemActions = new Actions(driver);
        itemActions.moveToElement(itemNameField).click().perform();
        itemActions.moveToElement(itemNameField).sendKeys(itemName).perform();
    }

    public void addPrivateNotes(String privateNotes) {
        privateNotesBlock.click();
        privateNotesInput.sendKeys(privateNotes);
    }

    public void save() {
        saveJobButton.click();
    }

    public void createCustomer(String firstName, String lastName) {
        newCustomerButton.click();
        newCustomerFirstName.sendKeys(firstName);
        newCustomerLastName.sendKeys(lastName);
        submitCustomer.click();
    }
}
