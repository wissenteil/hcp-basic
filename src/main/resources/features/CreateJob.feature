Feature: Create Job

  As a pro customer
  I want to create and schedule upcoming jobs
  So that I can project the workload and income upfront

  Background: Customer to be on a sing in page
    Given a customer is on sign in page

  Scenario: Customer to create job
    Given a logged in customer Johny Bravo
    When Johny creates following job for his new customer
      | Item name        | Private notes                           | Date from        | Date to          |
      | Fix sink leakage | What comes private shall remain private | Next working day | Next working day |
    Then job is saved for Johny as follows:
      | Item name        | Private notes                           |
      | Fix sink leakage | What comes private shall remain private |