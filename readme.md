#Welcome to HCP basic code challenge project
    
##To run Cucumber tests execute:
###gradle cucumberTest -Dspring.profiles.active=[environment],[browser]
###example: gradle cucumberTest -Dspring.profiles.active=qa,firefox

###Supported browsers:
- [x] firefox
- [x]  chrome (tests brake on it)
###Supported environments:
- [x] qa
- [x] ci (does not work)

###To execute tests against custom e.g. deployed from branch service you can override BASE_URL parameter:
- [x] Append -DBASE_URL=example.com to your run command
###Example: gradle cucumberTest -Dspring.profiles.active=qa,firefox -DBASE_URL=https://www.google.com  

    